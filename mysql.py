from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.schema import Column
from sqlalchemy.orm import sessionmaker
from sqlalchemy.types import String, DateTime, Integer
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Mail(Base):
	__tablename__ = 'mails'
	_id = Column(Integer, primary_key = True)
	_To = Column(String(50))
	_From = Column(String(50))
	_Content = Column(String(200))
	_Datetime = Column(DateTime)
	#_Status = Column(boolean)

	def __init__(self, To, From, Content, Datetime):
		self._To = To
		self._From = From
		self._Content = Content
		self._Datetime = Datetime
		
	def __repr__(self):
		return "<Mail(_To = '%s', _From = '%s', _Content = '%s', _Datetime = '%s')>"\
					% (self._To, self._From, self._Content, self._Datetime)

class Database(object):
	def __init__(self, dialect = "mysql", driver = "mysqldb", username = "jianywan",\
					   password = "wjy", host = "localhost", dbName = "mailDB"):
		url = '%s+%s://%s:%s@%s/%s' % (dialect, driver, username, password, host, dbName)
		self._engine = create_engine(url, echo = True)
		Base.metadata.create_all(self._engine)
		Session = sessionmaker(bind = self._engine)
		self._session = Session()
	
	#create a table
	#def create(self, tablename):
		
	def insert(self, entry):
		self._session.add(entry)
		self._session.commit()

	#def update(self):

	#def delete(self):

	#def dbDump(self):

#if __name__ == '__main__':
#	entry = Mail(To = 'jianywan@126.com', From = 'jianywan@gmail.com',
#				 Content = 'http://docs.sqlalchemy.org/en/rel_1_0/_modules/examples/adjacency_list/adjacency_list.html',
#				 Datetime = datetime.today())
#	db = Database(dbName = "mailDB")
#	db.insert(entry)
