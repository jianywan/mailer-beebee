#!/usr/bin/python
import re
import time
import mailer
import mysql
import datetime
import threading
import threadpool

MailHost = {"gmail":"smtp.gmail.com",
			"163":"smtp.163.com",
			"126":"smtp.126.com",
			"sina":"smtp.sina.com",
			"sohu":"smtp.sohu.com"}
MailPort = {"gmail":587,
			"163":25,
			"126":25,
			"sina":25,
			"sohu":25}
Maillink = "./src/mailer.html"

class Reader(threading.Thread):
	def __init__(self, readertype, sendpool, FilePos = ""):
		threading.Thread.__init__(self)
		self._readertype = readertype
		self._sendpool = sendpool
		self._FilePos = FilePos
		self._dismissed = threading.Event()

	def run(self):
		fp = open(self._FilePos)
		while True:
			if self._dismissed.isSet():
				break
			line = fp.readline()
			if len(line) == 0:
				break
			#initialize the sender
			line = line.strip('\n')
			if self._readertype == "WORK":
				if validateEmail(line) == False:
					continue
				request =  threadpool.WorkRequest(callable_ = do_sendwork,
											 	kwds = {"To":line},
											 	callback = do_keepwork)
				self._sendpool.putRequest(request)
			elif self._readertype == "INIT":
				str_split = line.split(' ')
				usr = str_split[0]
				pwd = str_split[1]
				if validateEmail(usr) == False:
					continue
				initrequest = threadpool.InitRequest(kwds = {"usr":usr, "pwd":pwd},
													callable_ = do_initwork)
				self._sendpool.putInitRequest(initrequest)
			
		fp.close()
			
	def dismiss(self):
		self._dismissed.set()

def validateEmail(address):
	if len(address) > 7:
		if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", address) != None:
			return True
	return False

def initmsg(_To, _From, _Subject, _Html):
	message = mailer.Message()
	message.To = _To
	message.From = _From
	message.Subject = _Subject
	message.Html = open(_Html, "rb").read()
	return message

def do_initwork(args = [], kwds = {}):
	host = MailHost["gmail"]
	port = MailPort["gmail"]
	usr = kwds["usr"]
	pwd = kwds["pwd"]
	#FIXME TLS or SSL
	con = mailer.Mailer(host, port, use_tls = True, usr = usr, pwd = pwd)
	con.connect(debug = False)
	return {"con":con, "From":usr}

def do_sendwork(args = [], kwds = {}):
	con = kwds["con"]
	From = kwds["From"]
	To = kwds["To"]
	msg = initmsg(_To = To, _From = From, _Subject = "a test html", _Html = "./src/mailer.html")
	con._send(con.server, msg)
	print "email to %s is sending...\n" % To
	Datetime = datetime.datetime.today()
	Status = "True"
	#here returns a dictionary & will be used in function do_keepwork
	return {"To":To, "From":From, "Datetime":Datetime, "Status":Status}

def do_keepwork(request, result):
	if isinstance(result, dict):
		To = result["To"]
		From = result["From"]
		Datetime = result["Datetime"]
		Status = result["Status"]
	print "sending to %s done by #%s\n" % (To, request.requestID)

if __name__ == '__main__':
	#start = time.clock()
	SendThread = threadpool.ThreadPool(5)
	rec_reader = Reader(readertype = "WORK", sendpool = SendThread, FilePos = "./src/tolist.txt")
	snd_reader = Reader(readertype = "INIT", sendpool = SendThread, FilePos = "./src/fromlist.txt")
	snd_reader.start()
	rec_reader.start()
	time.sleep(2)
	SendThread.wait()
	SendThread.dismissWorkers(5, do_join=True)
	#end = time.clock()
#	print "run time is: %f seconds" % (end-start)
